import 'dart:async';
import 'dart:convert';
import 'package:aqi/admin_page.dart';
import 'package:aqi/config.dart';
import 'package:aqi/gauge.dart';
import 'package:aqi/model/machine.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Polygons extends StatefulWidget {
  @override
  State<Polygons> createState() => MapSampleState();
}

class MapSampleState extends State<Polygons> with TickerProviderStateMixin {
  Completer<GoogleMapController> _controller = Completer();

  Machine machine;
  int color = -1;
  int colorAreas = -1;

  List<Color> colorArea = [
    Colors.blueAccent,
    Colors.green,
    Colors.yellow,
    Colors.deepOrange,
    Colors.red
  ];

  List<int> colorValue = [20, 44, 65, 135, 205];
  Set<Marker> _markers = {};
  Set<Polygon> _polygon = {};
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  int state = 0;

  void callMap() async {
    _markers = {};
    Response response =
        await get("${Config.urlHTTP}/machine", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());

    setState(() {
      machine = Machine.fromJson(jsonResponse);
      for (int i = 0; i < machine.message.data.length; i++) {
        color++;
        if (color == 5) {
          color = 0;
        }

        int values = colorValue[color];
        var _marker = Marker(
            onTap: () => {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => GaugePage(
                          uid: machine.message.data[i].uid,
                          value: values,
                          name: "${machine.message.data[i].name}")))
                },
            infoWindow: InfoWindow(
                title: "", snippet: "${machine.message.data[i].name}"),
            position: LatLng(
                machine.message.data[i].lat, machine.message.data[i].lng),
            markerId: MarkerId("${i}"));

        _markers.add(_marker);
      }

      _polygon = {};
      for (int i = 0; i < machine.message.data.length; i++) {
        colorAreas++;
        if (colorAreas == 5) {
          colorAreas = 0;
        }

        print("_polygon");
        List<LatLng> listLatLng = [];
        for (int j = 0; j < machine.message.data[i].area.length; j++) {
          print("_polygonj ${j} ${machine.message.data[i].area[j].lat}");

          if (machine.message.data[i].area[j].lat != null &&
              machine.message.data[i].area[j].lng != null) {
            listLatLng.add(LatLng(machine.message.data[i].area[j].lat,
                machine.message.data[i].area[j].lng));
          }
        }

        if (listLatLng.length >= 3) {
          listLatLng.add(listLatLng[0]);

          var poly = Polygon(
              polygonId: PolygonId("${i}"),
              strokeWidth: 2,
              strokeColor: colorArea[colorAreas],
              fillColor: colorArea[colorAreas].withOpacity(0.6),
              points: listLatLng);

          _polygon.add(poly);
          listLatLng = [];
        }
      }
    });
  }

  @override
  void initState() {
    getStateUser();
    callMap();
  }

  void getStateUser() async {
    final SharedPreferences prefs = await _prefs;

    setState(() {
      state = prefs.getInt("user");
    });
  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(6.751659, 100.324862),
    zoom: 10,
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: LightColors.kLightGreen,
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              callMap();
            },
          ),
          (state == 1)
              ? IconButton(
                  icon: Icon(Icons.settings),
                  onPressed: () {
                    AnimationController animationController =
                        AnimationController(
                      vsync: this,
                      duration: Duration(
                        milliseconds: 200,
                      ),
                      lowerBound: 0.0,
                      upperBound: 0.1,
                    )..addListener(() {
                            setState(() {});
                          });
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => AdminPage()));
                  },
                )
              : Container(),
        ],
        backgroundColor: LightColors.kGreen,
        title: Text("Map", style: TextStyle(fontFamily: 'Prompt')),
      ),
      body: GoogleMap(

        markers: _markers,
        polygons: _polygon,
        mapType: MapType.normal,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);

        },
      ),
    );
  }
}
