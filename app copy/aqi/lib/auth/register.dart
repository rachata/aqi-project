
import 'dart:convert';
import 'package:aqi/Apptheme.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';


class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController usrname =
  TextEditingController();

  TextEditingController password = TextEditingController();

  TextEditingController cmfPassword = TextEditingController();


  void register() async{

//    if(password.text.toString() == cmfPassword.text.toString()){
//      String url =  "http://34.87.185.176:8989";
//      final jsons =  jsonEncode({"username" : usrname.text.toString() , "password" : password.text.toString()});
//
//      Response response = await post("${url}/regis/patient"  ,
//          headers: <String, String>{
//            'Content-Type': 'application/json; charset=UTF-8',
//          },
//          body: jsons);
//
//      final jsonResponse = json.decode(response.body.toString());
//
//
//      if(jsonResponse['status'] == 200){
//        _showMyDialog("การสมัครสำเร็จ" , true);
//
//      }else{
//        _showMyDialog("การสมัครไม่สำเร็จ" , false);
//      }
//    }else{
//      _showMyDialog("Password ไม่ตรงกัน" , false);
//    }

  }

//  Future<void> _showMyDialog(String text , bool status) async {
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text('Register'),
//          content: SingleChildScrollView(
//            child: ListBody(
//              children: <Widget>[
//                Text("${text}"),
//              ],
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('ตกลง'),
//              onPressed: () {
//                if(status){
//                  Navigator.of(context)
//                      .pushReplacement(MaterialPageRoute(builder: (_) => LoginPage()));
//                }else{
//                  Navigator.of(context).pop();
//                }
//
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }


  @override
  Widget build(BuildContext context) {
    Widget title = Text(
      'Register',
      style: TextStyle(
          color: Colors.white,
          fontSize: 34.0,
          fontWeight: FontWeight.bold,
          shadows: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.15),
              offset: Offset(0, 5),
              blurRadius: 10.0,
            )
          ]),
    );

    Widget subTitle = Padding(
        padding: const EdgeInsets.only(right: 56.0),
        child: Text(
          '',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16.0,
          ),
        ));

    Widget registerButton = Positioned(
      left: MediaQuery.of(context).size.width / 4,
      bottom: 40,
      child: InkWell(
        onTap: () {
          register();
        },
        child: Container(
          width: MediaQuery.of(context).size.width / 2,
          height: 80,
          child: Center(
              child: new Text("Register",
                  style: const TextStyle(
                      color: const Color(0xfffefefe),
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      fontSize: 20.0))),
          decoration: BoxDecoration(
              gradient: AppTheme.mainButton,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  offset: Offset(0, 5),
                  blurRadius: 10.0,
                )
              ],
              borderRadius: BorderRadius.circular(9.0)),
        ),
      ),
    );

    Widget registerForm = Container(
      height: 300,
      child: Stack(
        children: <Widget>[
          Container(
            height: 220,
            width: MediaQuery.of(context).size.width,
            padding: const EdgeInsets.only(left: 32.0, right: 12.0),
            decoration: BoxDecoration(
                color: Color.fromRGBO(255, 255, 255, 0.8),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextField(

                    decoration: InputDecoration(

                        hintText: 'Username'
                    ),
                    controller: usrname,
                    style: TextStyle(fontSize: 16.0),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextField(
                    decoration: InputDecoration(

                        hintText: 'password'
                    ),

                    controller: password,
                    style: TextStyle(fontSize: 16.0),
                    obscureText: true,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: TextField(

                    decoration: InputDecoration(

                        hintText: 'Confirme password'
                    ),

                    controller: cmfPassword,
                    style: TextStyle(fontSize: 16.0),
                    obscureText: true,
                  ),
                ),
              ],
            ),
          ),
          registerButton,
        ],
      ),
    );

    Widget socialRegister = Column(
      children: <Widget>[

      ],
    );

    return Scaffold(

      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('assets/songkhla.jpg'),
                    fit: BoxFit.cover)
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: AppTheme.transparentGreen,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 28.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Spacer(flex:3),
                title,
                Spacer(),

                subTitle,
                Spacer(flex:2),

                registerForm,
                Spacer(flex:2),
                Padding(
                    padding: EdgeInsets.only(bottom: 20), child: socialRegister)
              ],
            ),
          ),

          Positioned(
            top: 35,
            left: 5,
            child: IconButton(
              color: Colors.white,
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}
