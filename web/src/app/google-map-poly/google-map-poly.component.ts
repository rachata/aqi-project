import { Component, OnInit } from '@angular/core';
import { LatLngLiteral, MouseEvent } from '@agm/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
interface marker {
  lat: number;
  lng: number;
  aqi : number;
  uid : string;
  url: any;
  label?: string;
  draggable: boolean;
}

interface poly {
  lat: number;
  lng: number;
  color : number
}

@Component({
  selector: 'app-google-map-poly',
  templateUrl: './google-map-poly.component.html',
  styleUrls: ['./google-map-poly.component.sass']
})
export class GoogleMapPolyComponent implements OnInit {

  colors = [
    "#3d5afe",
    "#00c853",
    "#ffeb3b",
    "#ff6d00",
    "#dd2c00"
  ];


  pinsColor = [{
    url: '/assets/pin1.png', 
    scaledSize: {
      height: 50,
      width: 50
    }
  } , {
    url: '/assets/pin2.png', 
    scaledSize: {
      height: 50,
      width: 50
    }
  }, {
    url: '/assets/pin3.png', 
    scaledSize: {
      height: 50,
      width: 50
    }
  } , {
    url: '/assets/pin4.png', 
    scaledSize: {
      height: 50,
      width: 50
    }
  } , {
    url: '/assets/pin5.png', 
    scaledSize: {
      height: 50,
      width: 50
    }
  } ]



  zoom: number = 16;
  lat: number = 7.202751
  lng: number = 100.591946

  paths = []
  markers: marker[] = [];


  icon = {
    url: '/assets/pin1.png', 
    scaledSize: {
      height: 40,
      width: 20
    }
  };


  ngOnInit() {

  }

  constructor(public router : Router  , private http: HttpClient) {
    this.callAPIPin();
  }

  colorSet(p){
    return this.colors[p[0]["color"]];
  }


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`);
  }

  select(m){
    
    this.router.navigateByUrl('/realtime/'+m["uid"]+"/"+m["aqi"]+"/"+m["label"]);

  }


  callAPIPin() {

    this.http.get("http://35.240.153.210:8989/machine", {})

      .subscribe(

        data => {
          var json = JSON.parse(JSON.stringify(data))

          for (var i = 0; i < Object.keys(json["message"]["data"]).length; i++) {


            var aqi = json["message"]["data"][i]["aqi"];

            
            var colorPin = 0;
            if (aqi  >= 0 && aqi <= 25) {
              colorPin = 0;
            } else if (aqi  >= 26 && aqi  <= 50) {
              colorPin = 1;
            } else if (aqi  >= 51 && aqi <= 100) {
              colorPin = 2;
            } else if (aqi >= 101 && aqi <= 200) {
              colorPin = 3;
            } else {
              colorPin = 4;
            }


            var markerRaw: marker =

            {
              url : this.pinsColor[colorPin],
              lat: json["message"]["data"][i]["lat"],
              lng: json["message"]["data"][i]["lng"],
              label: json["message"]["data"][i]["name"] + "\n" + "AQI : " + json["message"]["data"][i]["aqi"],
              aqi : json["message"]["data"][i]["aqi"],
              uid : json["message"]["data"][i]["uid"],
              draggable: false
            }

            var keyArea = Object.keys(json["message"]["data"][i]["area"])

            if (keyArea.length >= 3) {

              var rawArray = []

              for (var j = 0; j < keyArea.length; j++) {

              

                var color = 0;
                if (aqi  >= 0 && aqi <= 25) {
                  color = 0;
                } else if (aqi  >= 26 && aqi  <= 50) {
                  color = 1;
                } else if (aqi  >= 51 && aqi <= 100) {
                  color = 2;
                } else if (aqi >= 101 && aqi <= 200) {
                  color = 3;
                } else {
                  color = 4;
                }

                var areaRaw: poly =
                {
                  lat: json["message"]["data"][i]["area"][j]["lat"],
                  lng: json["message"]["data"][i]["area"][j]["lng"],
                  color : color
                }

                rawArray.push(areaRaw);
              }

              this.paths.push(rawArray);
            }
            this.markers.push(markerRaw);
          }
        },
        error => {
        }
      );
  }

}
