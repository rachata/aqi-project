import { HttpClient } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';


@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.sass']
})
export class MachineComponent implements OnInit {

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  data = [];
  filteredData = [];

  selectedOption: string;
  columns = [

    { prop: "name" },
    { prop: "uid" },
    { prop: "noti" },
    { prop: "rowNoti" }

  ];




  @ViewChild(DatatableComponent, { static: false }) table2: DatatableComponent;
  constructor(private http: HttpClient) {

  }


  dataList = [
  ]

  ngOnInit() {

    this.callAPI();

  }


  updateNotiType(row) {


    var notiRaw = 0;

    if(row["rowNoti"] == 0){
      notiRaw = 1;

    }
    var data =
    {
      "uid": row["uid"],
      "noti": notiRaw
    }

    console.log(JSON.stringify(data));


    this.http.post("http://35.240.153.210:8989/machine/noti", data)
      .subscribe(data => {

        console.log("status " , JSON.stringify(data));

        this.callAPI();
      })

  }
  callAPI() {

    this.dataList = [];
    this.filteredData = [];
    this.http.get("http://35.240.153.210:8989/machine", {})

      .subscribe(

        data => {

          var json = JSON.parse(JSON.stringify(data))

          console.log(JSON.stringify(json));

          if (json["status"] == 200) {

            for (var i = 0; i < Object.keys(json['message']["data"]).length; i++) {
              var raw = {};

              raw["name"] = json["message"]["data"][i]["name"]
              raw["uid"] = json["message"]["data"][i]["uid"]
              raw["rowNoti"] = json["message"]["data"][i]["noti_type"]


              if (json["message"]["data"][i]["noti_type"] == 0) {

                raw["noti"] = "แจ้งเตือน 2 เวลา"
              } else {
                raw["noti"] = "แจ้งเตือน 3 เวลา"
              }

              this.dataList.push(raw);
            }

            this.data = this.dataList;
            this.filteredData = this.dataList;

          }



        })
  }

  deleteRow(r) {

    console.log(JSON.stringify(r))

    var data =
    {
      "uid": r["uid"]
    }

    this.http.post("http://35.240.153.210:8989/machine/delete", data)
      .subscribe(data => {

        this.callAPI();
      })

  }

  arrayRemove(array, id) {
    return array.filter(function (element) {
      return element.id != id;
    });
  }

  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match
        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });

    console.log(JSON.stringify(this.data))
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
  getId(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

}


