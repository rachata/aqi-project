import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GaugeComponent } from './charts/gauge/gauge.component';
import { GenUidComponent } from './gen-uid/gen-uid.component';
import { GoogleMapAddComponent } from './google-map-add/google-map-add.component';
import { GoogleMapPolyComponent } from './google-map-poly/google-map-poly.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { GraphComponent } from './graph/graph.component';
import { MachineComponent } from './machine/machine.component';
import { RealtimeComponent } from './realtime/realtime.component';
import { SelectSettingComponent } from './select-setting/select-setting.component';
const routes: Routes = [

  {
    path: 'list-setting',
    component : SelectSettingComponent
  },

  {
    path: 'machine',
    component : MachineComponent
  },

  {
    path: 'graph/:uid',
    component : GraphComponent
  },

  {
    path: 'realtime/:uid/:aqi/:name',
    component : RealtimeComponent
  },

  {
    path: 'uid',
    component : GenUidComponent
  },

  {
    path: 'googlemap-add',
    component : GoogleMapAddComponent
  },

  {
    path: 'googlemap-poly',
    component : GoogleMapPolyComponent
  },
  {
    path: 'googlemap',
    component : GoogleMapComponent
  },
  {
    path: 'dashboard',
    loadChildren: () =>
      import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: 'email',
    loadChildren: () => import('./email/email.module').then(m => m.EmailModule)
  },
  {
    path: 'appointment',
    loadChildren: () =>
      import('./appointment/appointment.module').then(m => m.AppointmentModule)
  },
  {
    path: 'doctors',
    loadChildren: () =>
      import('./doctors/doctors.module').then(m => m.DoctorsModule)
  },
  {
    path: 'staff',
    loadChildren: () => import('./staff/staff.module').then(m => m.StaffModule)
  },
  {
    path: 'patient',
    loadChildren: () =>
      import('./patient/patient.module').then(m => m.PatientModule)
  },
  {
    path: 'payment',
    loadChildren: () =>
      import('./payment/payment.module').then(m => m.PaymentModule)
  },
  {
    path: 'room',
    loadChildren: () => import('./room/room.module').then(m => m.RoomModule)
  },
  {
    path: 'apps',
    loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule)
  },
  {
    path: 'widget',
    loadChildren: () =>
      import('./widget/widget.module').then(m => m.WidgetModule)
  },
  {
    path: 'ui',
    loadChildren: () => import('./ui/ui.module').then(m => m.UiModule)
  },
  {
    path: 'forms',
    loadChildren: () => import('./forms/forms.module').then(m => m.FormModule)
  },
  {
    path: 'tables',
    loadChildren: () =>
      import('./tables/tables.module').then(m => m.TablesModule)
  },
  {
    path: 'media',
    loadChildren: () => import('./media/media.module').then(m => m.MediaModule)
  },
  {
    path: 'charts',
    loadChildren: () =>
      import('./charts/charts.module').then(m => m.ChartsModule)
  },
  {
    path: 'timeline',
    loadChildren: () =>
      import('./timeline/timeline.module').then(m => m.TimelineModule)
  },
  {
    path: 'icons',
    loadChildren: () => import('./icons/icons.module').then(m => m.IconsModule)
  },
  {
    path: 'authentication',
    loadChildren: () =>
      import('./authentication/authentication.module').then(
        m => m.AuthenticationModule
      )
  },
  {
    path: 'extra-pages',
    loadChildren: () =>
      import('./extra-pages/extra-pages.module').then(m => m.ExtraPagesModule)
  },
  {
    path: 'maps',
    loadChildren: () => import('./maps/maps.module').then(m => m.MapsModule)
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
