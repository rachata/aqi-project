import { RouteInfo } from './sidebar.metadata';
export const ROUTES: RouteInfo[] = [
  {
    path: '',
    title: 'Map',
    icon: 'fas fa-map-marked-alt',
    class: 'menu-toggle',
    groupTitle: false,
    submenu: [
      // {
      //   path: '/googlemap',
      //   title: 'การแสดงผลแบบ Marker',
      //   icon: '',
      //   class: 'ml-menu',
      //   groupTitle: false,
      //   submenu: []
      // },
      {
        path: '/googlemap-poly',
        title: 'Map',
        icon: '',
        class: 'ml-menu',
        groupTitle: false,
        submenu: []
      },
     
    ]
  },
  {
    path: '',
    title: 'Setting',
    icon: 'fas fa-users-cog',
    class: 'menu-toggle',
    groupTitle: false,
    submenu: [

            {
        path: '/authentication',
        title: 'ตั้งค่า',
        icon: '',
        class: 'ml-menu',
        groupTitle: false,
        submenu: []
      },

      // {
      //   path: '/machine',
      //   title: 'เครื่องวัด',
      //   icon: '',
      //   class: 'ml-menu',
      //   groupTitle: false,
      //   submenu: []
      // },

      // {
      //   path: '/googlemap-add',
      //   title: 'เพิ่มเครื่องวัด',
      //   icon: '',
      //   class: 'ml-menu',
      //   groupTitle: false,
      //   submenu: []
      // },
      // {
      //   path: '/uid',
      //   title: 'UID',
      //   icon: '',
      //   class: 'ml-menu',
      //   groupTitle: false,
      //   submenu: []
      // }
    ]
  },
];
