import { Component, NgModule, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateModule } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as moment from 'moment';

import * as CanvasJS from '../../assets/canvasjs.min';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

export const MY_FORMATS = {
  parse: {
    dateInput: 'YYYY-MM',
  },
  display: {
    dateInput: 'YYYY-MM-DD',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};



@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.sass'],

  providers: [

    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class GraphComponent implements OnInit {


  chartPM25
  chartPM10
  chartNO2
  chartSO2
  chartCO
  chartO3

  listPM25 = [];
  listPM10 = [];

  listNO2 = [];
  listSO2 = [];

  listCO = [];
  listO3 = [];



  csv = []
  uid
  constructor(public http: HttpClient, public route: ActivatedRoute) {





    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))
      this.uid = params['uid']
    })

  }


  dt = new FormControl('')
  dtStart = new FormControl('');
  dtEnd = new FormControl('');

  tStart = new FormControl('');
  tEnd = new FormControl('');


  setUp(){

    this.chartPM25 = new CanvasJS.Chart("chartContainerPM25", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ PM25"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartPM25.render();

        }
      },

      data: [

        {
          color: "blue",
          type: "column",
          name: "PM2.5",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ug/m3",
          dataPoints: this.listPM25
        },

      ]
    });

    this.chartPM25.render();


    this.chartPM10 = new CanvasJS.Chart("chartContainerPM10", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ PM10"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartPM10.render();

        }
      },

      data: [

        {
          color: "red",
          type: "column",
          name: "PM10",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ug/m3",
          dataPoints: this.listPM10
        },

      ]
    });

    this.chartPM10.render();


    this.chartNO2 = new CanvasJS.Chart("chartContainerNO2", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ NO2"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartNO2.render();

        }
      },

      data: [

        {
          color: "green",
          type: "column",
          name: "NO2",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ppm",
          dataPoints: this.listNO2
        },

      ]
    });

    this.chartNO2.render();


    this.chartSO2 = new CanvasJS.Chart("chartContainerSO2", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ SO2"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartSO2.render();

        }
      },

      data: [

        {
          type: "column",
          name: "SO2",
          color: "brown",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ppm",
          dataPoints: this.listSO2
        },

      ]
    });

    this.chartSO2.render();


    this.chartCO = new CanvasJS.Chart("chartContainerCO", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ CO"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartCO.render();

        }
      },

      data: [

        {
          type: "column",
          name: "CO",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ppm",
          dataPoints: this.listCO,
          color: "orange"
        },

      ]
    });

    this.chartCO.render();


    this.chartO3 = new CanvasJS.Chart("chartContainerO3", {
      animationEnabled: true,
      exportEnabled: true,
      zoomEnabled: true,
      title: {
        text: "กราฟ O3"
      },

      legend: {
        cursor: "pointer",
        verticalAlign: "top",
        horizontalAlign: "center",
        dockInsidePlotArea: true,
        itemclick: (e) => {

          if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
          } else {
            e.dataSeries.visible = true;
          }
          this.chartO3.render();

        }
      },

      data: [

        {
          type: "column",
          name: "O3",
          showInLegend: true,
          markerSize: 10,
          yValueFormatString: "# ppm",
          dataPoints: this.listO3,
          color: "yellow"
        },

      ]
    });

    this.chartO3.render();



  }
  ngOnInit(): void {

    this.setUp();


  }

  dateSelect() {

    this.listPM25 = [];
    this.listPM10 = [];
    this.listNO2 = [];
    this.listSO2 = [];
    this.listCO = [];
    this.listO3 = [];

    this.setUp();

    console.log(moment(this.dt.value).format('YYYY-MM-DD'))


    this.http.get("http://35.240.153.210:8989/log/day/" + moment(this.dt.value).format('YYYY-MM-DD') + "/" + this.uid, {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {


            this.csv = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {
              var rawPM25 = {};
              var rawPm10 = {};
              var rawNO2 = {};
              var rawSO2 = {};
              var rawCO = {};
              var rawO3 = {};

              rawPM25["label"] = data["message"][i]["dtf"]
              rawPM25["y"] = data["message"][i]["pm25"];
              this.listPM25.push(rawPM25);

              rawPm10["label"] = data["message"][i]["dtf"]
              rawPm10["y"] = data["message"][i]["pm10"];
              this.listPM10.push(rawPm10);

              rawSO2["label"] = data["message"][i]["dtf"]
              rawSO2["y"] = data["message"][i]["so2"];
              this.listSO2.push(rawSO2);

              rawNO2["label"] = data["message"][i]["dtf"]
              rawNO2["y"] = data["message"][i]["no2"];
              this.listNO2.push(rawNO2);

              rawCO["label"] = data["message"][i]["dtf"]
              rawCO["y"] = data["message"][i]["co"];
              this.listCO.push(rawCO);

              rawO3["label"] = data["message"][i]["dtf"]
              rawO3["y"] = data["message"][i]["o3"];
              this.listO3.push(rawO3);




            }
            console.log(JSON.stringify(this.listO3));

            this.chartPM25.data[0] = this.listPM25;
            this.chartPM10.data[0] = this.listPM10;
            this.chartNO2.data[0] = this.listNO2;
            this.chartSO2.data[0] = this.listSO2;
            this.chartCO.data[0] = this.listCO;
            this.chartO3.data[0] = this.listO3;


            this.chartPM25.render();
            this.chartPM10.render();
            this.chartNO2.render();
            this.chartSO2.render();
            this.chartCO.render();
            this.chartO3.render();

          }
        }

      )



  }

  monthSelect() {

    this.listPM25 = [];
    this.listPM10 = [];
    this.listNO2 = [];
    this.listSO2 = [];
    this.listCO = [];
    this.listO3 = [];

    this.setUp();

    console.log(moment(this.dt.value).format('YYYY-MM-01'))


    this.http.get("http://35.240.153.210:8989/log/month/" + moment(this.dt.value).format('YYYY-MM-01') + "/" + this.uid, {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {

            this.csv = data["message"];

            for (var i = 0; i < Object.keys(data["message"]).length; i++) {
              var rawPM25 = {};
              var rawPm10 = {};
              var rawNO2 = {};
              var rawSO2 = {};
              var rawCO = {};
              var rawO3 = {};

              rawPM25["label"] = data["message"][i]["dtf"]
              rawPM25["y"] = data["message"][i]["pm25"];
              this.listPM25.push(rawPM25);

              rawPm10["label"] = data["message"][i]["dtf"]
              rawPm10["y"] = data["message"][i]["pm10"];
              this.listPM10.push(rawPm10);

              rawSO2["label"] = data["message"][i]["dtf"]
              rawSO2["y"] = data["message"][i]["so2"];
              this.listSO2.push(rawSO2);

              rawNO2["label"] = data["message"][i]["dtf"]
              rawNO2["y"] = data["message"][i]["no2"];
              this.listNO2.push(rawNO2);

              rawCO["label"] = data["message"][i]["dtf"]
              rawCO["y"] = data["message"][i]["co"];
              this.listCO.push(rawCO);

              rawO3["label"] = data["message"][i]["dtf"]
              rawO3["y"] = data["message"][i]["o3"];
              this.listO3.push(rawO3);




            }
            console.log(JSON.stringify(this.listO3));

            this.chartPM25.data[0] = this.listPM25;
            this.chartPM10.data[0] = this.listPM10;
            this.chartNO2.data[0] = this.listNO2;
            this.chartSO2.data[0] = this.listSO2;
            this.chartCO.data[0] = this.listCO;
            this.chartO3.data[0] = this.listO3;


            this.chartPM25.render();
            this.chartPM10.render();
            this.chartNO2.render();
            this.chartSO2.render();
            this.chartCO.render();
            this.chartO3.render();

          }
        }

      )

  }

  yearSelect() {


    this.listPM25 = [];
    this.listPM10 = [];
    this.listNO2 = [];
    this.listSO2 = [];
    this.listCO = [];
    this.listO3 = [];
    
    this.setUp();

    console.log(moment(this.dt.value).format('YYYY'))


    this.http.get("http://35.240.153.210:8989/log/year/" + moment(this.dt.value).format('YYYY') + "/" + this.uid, {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {

            this.csv = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {
              var rawPM25 = {};
              var rawPm10 = {};
              var rawNO2 = {};
              var rawSO2 = {};
              var rawCO = {};
              var rawO3 = {};

              rawPM25["label"] = data["message"][i]["dtf"]
              rawPM25["y"] = data["message"][i]["pm25"];
              this.listPM25.push(rawPM25);

              rawPm10["label"] = data["message"][i]["dtf"]
              rawPm10["y"] = data["message"][i]["pm10"];
              this.listPM10.push(rawPm10);

              rawSO2["label"] = data["message"][i]["dtf"]
              rawSO2["y"] = data["message"][i]["so2"];
              this.listSO2.push(rawSO2);

              rawNO2["label"] = data["message"][i]["dtf"]
              rawNO2["y"] = data["message"][i]["no2"];
              this.listNO2.push(rawNO2);

              rawCO["label"] = data["message"][i]["dtf"]
              rawCO["y"] = data["message"][i]["co"];
              this.listCO.push(rawCO);

              rawO3["label"] = data["message"][i]["dtf"]
              rawO3["y"] = data["message"][i]["o3"];
              this.listO3.push(rawO3);




            }
            console.log(JSON.stringify(this.listO3));

            this.chartPM25.data[0] = this.listPM25;
            this.chartPM10.data[0] = this.listPM10;
            this.chartNO2.data[0] = this.listNO2;
            this.chartSO2.data[0] = this.listSO2;
            this.chartCO.data[0] = this.listCO;
            this.chartO3.data[0] = this.listO3;


            this.chartPM25.render();
            this.chartPM10.render();
            this.chartNO2.render();
            this.chartSO2.render();
            this.chartCO.render();
            this.chartO3.render();

          }
        }

      )


  }

  selectAvd() {


    var dStart =(moment(this.dtStart.value).format('YYYY-MM-DD'))
    var ts =(this.tStart.value);

    var start = dStart+" " + ts

    var dEnd =(moment(this.dtEnd.value).format('YYYY-MM-DD'))
    var te =(this.tEnd.value);

    var end = dEnd+" " + te


    console.log(start + " - " + end);

    this.listPM25 = [];
    this.listPM10 = [];
    this.listNO2 = [];
    this.listSO2 = [];
    this.listCO = [];
    this.listO3 = [];
    
    this.setUp();

    console.log(moment(this.dt.value).format('YYYY'))


    this.http.get("http://35.240.153.210:8989/log/advanced/" + start + "/" + end + "/" + this.uid, {})

      .subscribe(

        data => {

          console.log(JSON.stringify(data))

                  

          if (data["status"] == 200) {

            this.csv = data["message"];
            for (var i = 0; i < Object.keys(data["message"]).length; i++) {
              var rawPM25 = {};
              var rawPm10 = {};
              var rawNO2 = {};
              var rawSO2 = {};
              var rawCO = {};
              var rawO3 = {};

              rawPM25["label"] = data["message"][i]["dtf"]
              rawPM25["y"] = data["message"][i]["pm25"];
              this.listPM25.push(rawPM25);

              rawPm10["label"] = data["message"][i]["dtf"]
              rawPm10["y"] = data["message"][i]["pm10"];
              this.listPM10.push(rawPm10);

              rawSO2["label"] = data["message"][i]["dtf"]
              rawSO2["y"] = data["message"][i]["so2"];
              this.listSO2.push(rawSO2);

              rawNO2["label"] = data["message"][i]["dtf"]
              rawNO2["y"] = data["message"][i]["no2"];
              this.listNO2.push(rawNO2);

              rawCO["label"] = data["message"][i]["dtf"]
              rawCO["y"] = data["message"][i]["co"];
              this.listCO.push(rawCO);

              rawO3["label"] = data["message"][i]["dtf"]
              rawO3["y"] = data["message"][i]["o3"];
              this.listO3.push(rawO3);




            }
            console.log(JSON.stringify(this.listO3));

            this.chartPM25.data[0] = this.listPM25;
            this.chartPM10.data[0] = this.listPM10;
            this.chartNO2.data[0] = this.listNO2;
            this.chartSO2.data[0] = this.listSO2;
            this.chartCO.data[0] = this.listCO;
            this.chartO3.data[0] = this.listO3;


            this.chartPM25.render();
            this.chartPM10.render();
            this.chartNO2.render();
            this.chartSO2.render();
            this.chartCO.render();
            this.chartO3.render();

          }
        }

      )



  }


  export(){

    this.downloadFile(this.csv);

  }
  downloadFile(data, filename = 'data') {
    let csvData = this.ConvertToCSV(data, ['id_machine', 'pm10', 'pm25', 'so2', 'o3', 'co', 'no2' , 'dtf']);
    console.log(csvData)
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'S.No,';

    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];

        line += ',' + array[i][head];
      }
      str += line + '\r\n';
    }
    return str;
  }


}
