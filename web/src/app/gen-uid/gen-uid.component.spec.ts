import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenUidComponent } from './gen-uid.component';

describe('GenUidComponent', () => {
  let component: GenUidComponent;
  let fixture: ComponentFixture<GenUidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenUidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenUidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
