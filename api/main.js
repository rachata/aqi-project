var app = require('express')();
var mysql = require('mysql');
var crypto = require('crypto');
var request = require('request')
var dateFormat = require('dateformat');
var bodyParser = require('body-parser');
const { exit } = require('process');

var port = process.env.PORT || 8989;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

var con = mysql.createConnection({
  host: "35.240.153.210",
  user: "win",
  password: "@#WinDB2020",
  database: "win"
});

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});


con.connect(function (err) {
  if (err) throw err;
});




app.post('/machine/noti', function (req, res) {


  var noti = req.body.noti;
  var uid = req.body.uid

  var query = "update machine set noti_type = "+noti+" WHERE uid = '"+uid+"' ";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200})
  })

})





app.post('/machine', function (req, res) {

  var name = req.body.name;
  var lat = req.body.lat;
  var lng = req.body.lng;
  var uid = req.body.uid;
  var line = req.body.line;

  var area = req.body.area;


  console.log(JSON.stringify(req.body));
  var query = "insert into machine (token_line , uid , name , lat , lng) values ( '" + line + "' , '" + uid + "' , '" + name + "' , " + lat + " , " + lng + ")";
  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    var idMachine = result.insertId

    if (Object.keys(area).length >= 1) {

      for (var i = 0; i < Object.keys(area).length; i++) {
        var latArea = area[i]['lat'];
        var lngArea = area[i]['lng'];

        var query = "insert into area_machine (id_machine , lat , lng) values (" + idMachine + " , " + latArea + " , " + lngArea + ")";

        con.query(query, function (err, result, fields) {
          if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
          }
        });

      }
      res.json({ "status": 200 })

    } else {
      res.json({ "status": 200 })
    }
  });
});

app.get('/log/last/:uid/:limit', function (req, res) {

  var uid = req.params.uid;
  var limit = req.params.limit;


  var query = "select log_machine.id_machine , log_machine.pm25 , log_machine.pm10 , log_machine.so2 , log_machine.o3 , log_machine.co , log_machine.no2 , log_machine.aqi ,  DATE_FORMAT(log_machine.dt, '%T') as 'dt' from log_machine inner join machine on machine.id = log_machine.id_machine WHERE machine.uid = '"+uid+"' order by log_machine.id desc limit " + limit

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {
      res.json({ "status": 200, "message": result })
    }else{
      res.json({ "status": 204})
    }

  })


})





app.post('/area', function (req, res) {

  var id = req.body.id;
  var area = req.body.area;


  if (Object.keys(area).length >= 1) {

    for (var i = 0; i < Object.keys(area).length; i++) {
      var latArea = area[i]['lat'];
      var lngArea = area[i]['lng'];

      var query = "insert into area_machine (id_machine , lat , lng) values (" + id + " , " + latArea + " , " + lngArea + ")";

      con.query(query, function (err, result, fields) {
        if (err) {
          console.log(err.stack);
          res.json({ "status": 500, message: err.stack })
          return;
        }
      });

    }
    res.json({ "status": 200 })

  } else {
    res.json({ "status": 200 })
  }
});


app.post('/machine/delete', function (req, res) {

  var uid = req.body.uid;



  var query = "select id from machine where uid = '" + uid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]["id"]

      var query = "delete from log_machine where id_machine = " + id;

      con.query(query, function (err, result, fields) {
        if (err) {
          console.log(err.stack);
          res.json({ "status": 500, message: err.stack })
          return;
        }

        var query = "delete from area_machine where id_machine =  " + id;

        con.query(query, function (err, result, fields) {
          if (err) {
            console.log(err.stack);
            res.json({ "status": 500, message: err.stack })
            return;
          }

          var query = "delete from machine where id = " + id;

          con.query(query, function (err, result, fields) {
            if (err) {
              console.log(err.stack);
              res.json({ "status": 500, message: err.stack })
              return;
            }

            res.json({ "status": 204 })

          })


        })


      })
    } else {
      res.json({ "status": 204 })
    }

  });
})


app.post('/area/delete', function (req, res) {

  var name = req.body.name;



  var query = "delete from machine WHERE name = '" + name + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }
    res.json({ "status": 200 })
  });
});

app.get('/machine', function (req, res) {

  var query = "select machine.noti_type ,  machine.uid  ,  machine.aqi , name , machine.lat as mlat , machine.lng as mlng , status , last_connect , area_machine.lat , area_machine.lng  from machine left join area_machine on machine.id = area_machine.id_machine";

  var listMachine = [];
  var nameMachine = [];
  var resultMechine = {};
  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    for (var i = 0; i < Object.keys(result).length; i++) {


      var temp = {};
      if (nameMachine.length == 0) {

        nameMachine.push(result[i]['name'])

        temp['name'] = result[i]['name']
        temp['lat'] = result[i]['mlat']
        temp['lng'] = result[i]['mlng']
        temp['status'] = result[i]['status']
        temp['lastConnect'] = result[i]['last_connect']
        temp['aqi'] = result[i]["aqi"];
        temp['uid'] = result[i]["uid"];
	temp['noti_type'] = result[i]["noti_type"];



        var listArea = [];
        var tempArea = {};

        tempArea['lat'] = result[i]['lat']
        tempArea['lng'] = result[i]['lng']

        listArea.push(tempArea)

        temp['area'] = listArea;
        listMachine.push(temp)

      } else {

        if (!nameMachine.includes(result[i]['name'])) {

          nameMachine.push(result[i]['name'])

          var temp = {};

          temp['name'] = result[i]['name']
          temp['lat'] = result[i]['mlat']
          temp['lng'] = result[i]['mlng']
          temp['status'] = result[i]['status']
          temp['aqi'] = result[i]["aqi"];
          temp['uid'] = result[i]["uid"];
          temp['lastConnect'] = result[i]['last_connect']
	  temp['noti_type'] = result[i]["noti_type"];

          var listArea = [];
          var tempArea = {};

          tempArea['lat'] = result[i]['lat']
          tempArea['lng'] = result[i]['lng']

          listArea.push(tempArea)

          temp['area'] = listArea;

          listMachine.push(temp)
        } else {

          var tempArea = {};

          tempArea['lat'] = result[i]['lat']
          tempArea['lng'] = result[i]['lng']

          for (var j = 0; j < Object.keys(listMachine).length; j++) {

            if (listMachine[j]['name'] == result[i]['name']) {

              listMachine[j]['area'].push(tempArea);
            }
          }
        }

      }
    }



    resultMechine['data'] = listMachine
    res.json({ "status": 200, message: resultMechine })
  });


})


app.get('/uuid', function (req, res) {

  var query = "SELECT UUID() as uid";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200, "uid": result[0]['uid'] })

  });
})

app.post('/uuid', function (req, res) {

  var uuid = req.body.uuid;
  var note = req.body.note;


  var query = "select * from uid  WHERE text = '" + uuid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {
      res.json({ "status": 204 })
    } else {

      var query = "insert into uid (text , note) values ('" + uuid + "' , '" + note + "')";
      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }


        res.json({ "status": 200 })
      });
    }


  });

})


app.get('/uid', function (req, res) {

  var query = "select * from uid";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200, "message": result })

  });
})

app.get('/uid/empty', function (req, res) {

  var query = "select * from uid where status = false";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    res.json({ "status": 200, "message": result })

  });
})




app.post('/uuid/delete', function (req, res) {

  var text = req.body.text;



  var query = "delete from uid WHERE text = '" + text + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }
    res.json({ "status": 200 })
  });
});

app.post('/log', function (req, res) {



  var obj = JSON.parse(JSON.stringify(JSON.parse(req.body.log)))


  console.log("/log " + JSON.stringify(obj));
  if (!obj.hasOwnProperty("uid")) {
    res.json({ "status": 204, "message": "Not successful, no uid" })

    return;
  }

  if (!obj.hasOwnProperty("pm25")) {
    res.json({ "status": 204, "message": "Not successful, no pm25" })
    return;
  }


  if (!obj.hasOwnProperty("pm10")) {
    res.json({ "status": 204, "message": "Not successful, no pm10" })
    return;
  }


  if (!obj.hasOwnProperty("so2")) {
    res.json({ "status": 204, "message": "Not successful, no so2" })
    return;
  }


  if (!obj.hasOwnProperty("o3")) {
    res.json({ "status": 204, "message": "Not successful, no o3" })
    return;
  }


  if (!obj.hasOwnProperty("aqi")) {
    res.json({ "status": 204, "message": "Not successful, no aqi" })
    return;
  }

  if (!obj.hasOwnProperty("co")) {
    res.json({ "status": 204, "message": "Not successful, no co" })
    return;
  }

  if (!obj.hasOwnProperty("no2")) {
    res.json({ "status": 204, "message": "Not successful, no no2" })
    return;
  }

  var uid = obj.uid;
  var pm25 = obj.pm25;
  var pm10 = obj.pm10;
  var so2 = obj.so2;
  var o3 = obj.o3;
  var co = obj.co;
  var no2 = obj.no2;
  var aqi = obj.aqi;
  var query = "select * from machine  WHERE uid = '" + uid + "'";

  con.query(query, function (err, result, fields) {

    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

	console.log(Object.keys(result).length);

    if (Object.keys(result).length >= 1) {

      var id = result[0]['id'];
      var query = "insert into log_machine (aqi , id_machine , pm25 , pm10 ,so2 , o3 , co , no2) values (" + aqi + "," + id + " , " + pm25 + " , " + pm10 + " ," + so2 + " , " + o3 + ", " + co + " ," + no2 + ")"
	console.log(query)
      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }

        var query = "update machine set aqi = " + aqi + " WHERE uid = '" + uid + "'";

        con.query(query, function (err, result, fields) {
          if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
          }
          res.json({ "status": 200, message: "success" });
        })

      })
    } else {
      res.json({ "status": 204, "message": "The UID has not been paired yet." })
    }
  });

});


app.post('/v2/log', function (req, res) {


  console.log("/v2/log " + JSON.stringify(req.body));
  if (!req.body.hasOwnProperty("uid")) {
    res.json({ "status": 204, "message": "Not successful, no uid" })

    return;
  }

  if (!req.body.hasOwnProperty("pm25")) {
    res.json({ "status": 204, "message": "Not successful, no pm25" })
    return;
  }


  if (!req.body.hasOwnProperty("pm10")) {
    res.json({ "status": 204, "message": "Not successful, no pm10" })
    return;
  }


  if (!req.body.hasOwnProperty("so2")) {
    res.json({ "status": 204, "message": "Not successful, no so2" })
    return;
  }


  if (!req.body.hasOwnProperty("o3")) {
    res.json({ "status": 204, "message": "Not successful, no o3" })
    return;
  }


  if (!req.body.hasOwnProperty("co")) {
    res.json({ "status": 204, "message": "Not successful, no co" })
    return;
  }

  if (!req.body.hasOwnProperty("no2")) {
    res.json({ "status": 204, "message": "Not successful, no no2" })
    return;
  }

  var uid = req.body.uid;
  var pm25 = req.body.pm25;
  var pm10 = req.body.pm10;
  var so2 = req.body.so2;
  var o3 = req.body.o3;
  var co = req.body.co;
  var no2 = req.body.no2;

  var query = "select * from machine  WHERE uid = '" + uid + "'";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]['id'];
      var query = "insert into log_machine (id_machine , pm25 , pm10 ,so2 , o3 , co , no2) values (" + id + " , " + pm25 + " , " + pm10 + " ," + so2 + " , " + o3 + ", " + co + " ," + no2 + ")"

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }
        res.json({ "status": 200, message: "success" });
      })
    } else {
      res.json({ "status": 204, "message": "The UID has not been paired yet." })
    }
  });

});



app.get('/log/:uid/:limit', function (req, res) {

  console.log(JSON.stringify(req.params))
  var query = "select * from machine where uid = '" + req.params.uid + "'";

  console.log(query);

  con.query(query, function (err, result, fields) {
    if (err) {
      console.log(err.stack);
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var query = "select *from log_machine where id_machine = " + result[0]['id'] + " ORDER BY id DESC LIMIT " + req.params.limit;


      con.query(query, function (err, result, fields) {
        if (err) {
          console.log(err.stack);
          res.json({ "status": 500, message: err.stack })
          return;
        }

        res.json(result)
      })

    } else {
      res.json({ "status": 204, "message": "No Data" })
    }


  });

})


app.get('/log/day/:day/:uid', function (req, res) {

  var day = req.params.day;
  var uid = req.params.uid;


  var query = "select id from machine where uid = '" + uid + "'"


  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]["id"]

      var query = "SELECT  id_machine , ROUND(avg(pm10) , 3) as pm10 , ROUND(avg(pm25) , 3)  as pm25 , ROUND(avg(so2) , 3)  as so2, ROUND(avg(o3) , 3) as o3, ROUND(avg(co) , 3) as co , ROUND(avg(no2) , 3) as no2 , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_machine  where dt between '" + day + " 00:00:00' AND '" + day + " 23:59:59' AND id_machine = " + id + " GROUP BY id_machine , dtf order by dtf  ASC ";

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }

        for (var i = 0; i < Object.keys(result).length; i++) {
          var time = dateFormat(result[i]['dtf'] + ":00:00", "HH")
          result[i]['dtf'] = time;
        }

        res.json({ "status": 200, "message": result })
      });

    } else {
      res.json({ "status": 204, "message": result })
    }

  })

})


app.get('/log/month/:month/:uid', function (req, res) {

  var month = req.params.month;
  var uid = req.params.uid;


  var query = "select id from machine where uid = '" + uid + "'"

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]["id"]
      var query = "SELECT id_machine , ROUND(avg(pm10) , 3) as pm10 , ROUND(avg(pm25) , 3)  as pm25 , ROUND(avg(so2) , 3)  as so2, ROUND(avg(o3) , 3) as o3, ROUND(avg(co) , 3) as co , ROUND(avg(no2) , 3) as no2 , DATE_FORMAT(dt,'%Y-%m-%d') as dtf  FROM log_machine where dt between '" + month + "' AND CONCAT(LAST_DAY('" + month + "')) AND id_machine = " + id + " GROUP BY id_machine ,  dtf order by dtf  ASC ";

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }

        for (var i = 0; i < Object.keys(result).length; i++) {
          var time = dateFormat(result[i]['dtf'] + ":00:00", "dd")
          result[i]['dtf'] = time;
        }

        res.json({ "status": 200, "message": result })
      });

    } else {
      res.json({ "status": 204, "message": result })
    }
  })



})


app.get('/log/year/:year/:uid', function (req, res) {

  var year = req.params.year;

  var uid = req.params.uid;

  var query = "select id from machine where uid = '" + uid + "'"

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]["id"]

      var query = "SELECT id_machine , ROUND(avg(pm10) , 3) as pm10 , ROUND(avg(pm25) , 3)  as pm25 , ROUND(avg(so2) , 3)  as so2, ROUND(avg(o3) , 3) as o3, ROUND(avg(co) , 3) as co , ROUND(avg(no2) , 3) as no2 , DATE_FORMAT(dt,'%Y-%m') as dtf  FROM log_machine  where dt between '" + year + "-01-01' AND '" + year + "-12-31'  AND id_machine = " + id + " GROUP BY id_machine , dtf order by dtf  ASC ";

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }

        for (var i = 0; i < Object.keys(result).length; i++) {
          var time = dateFormat(result[i]['dtf'] + "-01 00:00:00", "mm")
          result[i]['dtf'] = time;
        }

        res.json({ "status": 200, "message": result })
      });


    } else {
      res.json({ "status": 204, "message": result })
    }
  })



})


app.get('/log/advanced/:start/:end/:uid', function (req, res) {

  var start = req.params.start;
  var end = req.params.end;

  var uid = req.params.uid;

  var query = "select id from machine where uid = '" + uid + "'"

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }

    if (Object.keys(result).length >= 1) {

      var id = result[0]['id'];

      var query = "SELECT id_machine ,  ROUND(avg(pm10) , 3) as pm10 , ROUND(avg(pm25) , 3)  as pm25 , ROUND(avg(so2) , 3)  as so2, ROUND(avg(o3) , 3) as o3, ROUND(avg(co) , 3) as co , ROUND(avg(no2) , 3) as no2 , DATE_FORMAT(dt,'%Y-%m-%d %H') as dtf  FROM log_machine  where dt between '" + start + "' AND '" + end + "'  AND id_machine = " + id + " GROUP BY id_machine , dtf order by dtf  ASC ";

      con.query(query, function (err, result, fields) {
        if (err) {
          res.json({ "status": 500, message: err.stack })
          return;
        }

        for (var i = 0; i < Object.keys(result).length; i++) {
          var time = dateFormat(result[i]['dtf'] + ":00:00", "dd/mm/yyyy HH")
          result[i]['dtf'] = time;
        }

        res.json({ "status": 200, "message": result })
      });


    } else {

      res.json({ "status": 204, "message": result })
    }

  })

})




app.get('/log/year', function (req, res) {

  var query = "SELECT DATE_FORMAT(dt,'%Y') as dtf  FROM log_machine  GROUP BY dtf order by dtf  ASC  ";

  con.query(query, function (err, result, fields) {
    if (err) {
      res.json({ "status": 500, message: err.stack })
      return;
    }
    res.json({ "status": 200, "message": result })
  });
})














app.listen(port, function () {
  console.log('Starting node.js on port ' + port);
});

