class Machine {
  int status;
  Message message;

  Machine({this.status, this.message});

  Machine.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message =
    json['message'] != null ? new Message.fromJson(json['message']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.message != null) {
      data['message'] = this.message.toJson();
    }
    return data;
  }
}

class Message {
  List<Data> data;

  Message({this.data});

  Message.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String name;
  double lat;
  double lng;
  int status;
  Null lastConnect;
  double aqi;
  String uid;
  int notiType;
  List<Area> area;

  Data(
      {this.name,
        this.lat,
        this.lng,
        this.status,
        this.lastConnect,
        this.aqi,
        this.uid,
        this.notiType,
        this.area});

  Data.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    lat = json['lat'];
    lng = json['lng'];
    status = json['status'];
    lastConnect = json['lastConnect'];
    aqi = json['aqi'] / 1.0;
    uid = json['uid'];
    notiType = json['noti_type'];
    if (json['area'] != null) {
      area = new List<Area>();
      json['area'].forEach((v) {
        area.add(new Area.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['status'] = this.status;
    data['lastConnect'] = this.lastConnect;
    data['aqi'] = this.aqi;
    data['uid'] = this.uid;
    data['noti_type'] = this.notiType;
    if (this.area != null) {
      data['area'] = this.area.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Area {
  double lat;
  double lng;

  Area({this.lat, this.lng});

  Area.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}

// class Machine {
//   int status;
//   Message message;
//
//   Machine({this.status, this.message});
//
//   Machine.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     message =
//     json['message'] != null ? new Message.fromJson(json['message']) : null;
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     if (this.message != null) {
//       data['message'] = this.message.toJson();
//     }
//     return data;
//   }
// }
//
// class Message {
//   List<Data> data;
//
//   Message({this.data});
//
//   Message.fromJson(Map<String, dynamic> json) {
//     if (json['data'] != null) {
//       data = new List<Data>();
//       json['data'].forEach((v) {
//         data.add(new Data.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     if (this.data != null) {
//       data['data'] = this.data.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Data {
//   String name;
//   String uid;
//   double lat;
//   double lng;
//   int status;
//   String lastConnect;
//   List<Area> area;
//
//   Data(
//       {this.name,
//         this.lat,
//         this.lng,
//         this.status,
//         this.lastConnect,
//         this.area,this.uid});
//
//   Data.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//     lat = json['lat'];
//     lng = json['lng'];
//     uid = json['uid'];
//     status = json['status'];
//     lastConnect = json['lastConnect'];
//     if (json['area'] != null) {
//       area = new List<Area>();
//       json['area'].forEach((v) {
//         area.add(new Area.fromJson(v));
//       });
//     }
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['name'] = this.name;
//     data['lat'] = this.lat;
//     data['lng'] = this.lng;
//     data['status'] = this.status;
//     data['uid'] = this.uid;
//     data['lastConnect'] = this.lastConnect;
//     if (this.area != null) {
//       data['area'] = this.area.map((v) => v.toJson()).toList();
//     }
//     return data;
//   }
// }
//
// class Area {
//   double lat;
//   double lng;
//
//   Area({this.lat, this.lng});
//
//   Area.fromJson(Map<String, dynamic> json) {
//     lat = json['lat'];
//     lng = json['lng'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['lat'] = this.lat;
//     data['lng'] = this.lng;
//     return data;
//   }
// }