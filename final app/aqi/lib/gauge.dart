import 'dart:async';
import 'dart:convert';
import 'package:aqi/config.dart';
import 'package:aqi/model/last_log.dart';
import 'package:http/http.dart';
import 'package:intl/intl.dart';
import 'package:aqi/app_properties.dart';
import 'package:aqi/detail.dart';
import 'package:aqi/model/raw.dart';
import 'package:aqi/mqtt_utility.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';
import 'package:mqtt_client/mqtt_client.dart' as mqtt;

class GraphDataGauge {
  GraphDataGauge(this.x, this.y);

  final String x;
  final double y;
}

class GaugePage extends StatefulWidget {
  final String name;
  int value;
  final String uid;

   GaugePage({Key key, this.name, this.value, this.uid}) : super(key: key);

  @override
  State<GaugePage> createState() => GaugePageState();
}

class GaugePageState extends State<GaugePage> {
  double o3 = 0;
  double co2 = 0;
  double no2 = 0;
  double so2 = 0;
  double pm25 = 0;
  double pm10 = 0;

  Future<mqtt.MqttClient> client;
  StreamSubscription subscription;
  MQTTUtility _mqttUtility;

  List<String> asset = [
    "assets/aqi01.png",
    "assets/aqi02.png",
    "assets/aqi03.png",
    "assets/aqi04.png",
    "assets/aqi05.png"
  ];
  int state = 0;
  String imageName = "assets/aqi01.png";

  String datetime = "";

  int i = 0;

  bool _valuePM25 = false;


  String  aqi;


  List<GraphDataGauge> linePM25 = [];
  List<GraphDataGauge> linePM10 = [];

  void getLast() async {

    print("${Config.urlHTTP}/log/last/${widget.uid}/10");

    Response response = await get("${Config.urlHTTP}/log/last/${widget.uid}/10",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponse = json.decode(response.body.toString());

    setState(() {

      if(jsonResponse["status"] == 200){


        aqi = "${jsonResponse["message"][0]["aqi"]}";
        datetime =  jsonResponse["message"][0]["dt"];


        if (double.parse(aqi) >= 0 && double.parse(aqi) <= 25) {
          imageName = asset[0];
        } else if (double.parse(aqi) >= 26 && double.parse(aqi) <= 37) {
          imageName = asset[1];
        } else if (double.parse(aqi) >= 38 && double.parse(aqi) <= 50) {
          imageName = asset[2];
        } else if (double.parse(aqi) >= 51 && double.parse(aqi) <= 90) {
          imageName = asset[3];
        } else {
          imageName = asset[4];
        }



        pm25 = jsonResponse["message"][0]["pm25"] / 1.0;
        pm10 = jsonResponse["message"][0]["pm10"] / 1.0;


        for(int i = jsonResponse["message"].length - 1; i >= 0 ;i--){

          linePM25.add(GraphDataGauge("${jsonResponse["message"][i]["dt"]}", jsonResponse["message"][i]["pm25"] / 1.0));
          linePM10.add(GraphDataGauge("${jsonResponse["message"][i]["dt"]}", jsonResponse["message"][i]["pm10"] / 1.0));

        }

      }


    });
  }

  void getMessage(String message, String topic) {
    final DateTime now = DateTime.now();

    final DateFormat formatter = DateFormat('dd/MM/yyyy HH:mm:ss');

    setState(() {
      datetime = formatter.format(now);
    });

    final jsonResponse = json.decode(message.toString());

    setState(() {

      aqi = "${jsonResponse['aqi']}";

      if (jsonResponse['aqi'] >= 0 && jsonResponse['aqi'] <= 25) {
        imageName = asset[0];
      } else if (jsonResponse['aqi'] >= 26 && jsonResponse['aqi'] <= 37) {
        imageName = asset[1];
      } else if (jsonResponse['aqi'] >= 38 && jsonResponse['aqi'] <= 50) {
        imageName = asset[2];
      } else if (jsonResponse['aqi'] >= 51 && jsonResponse['aqi'] <= 90) {
        imageName = asset[3];
      } else {
        imageName = asset[4];
      }


      if (jsonResponse['pm25'] != null) {

        pm25 = jsonResponse['pm25'] / 1.0;
        addDataPM25(pm25);

      }

      if (jsonResponse['pm10'] != null) {
        pm10 = jsonResponse['pm10'] / 1.0;
        addDataPM10(pm10);
      }

      if (jsonResponse['o3'] != null) {
        o3 = jsonResponse['o3'] / 1.0;
      }

      if (jsonResponse['co'] != null) {
        co2 = jsonResponse['co'] / 1.0;
      }

      if (jsonResponse['no2'] != null) {
        no2 = jsonResponse['no2'];
      }

      if (jsonResponse['so2']!= null) {
        so2 = jsonResponse['so2'] / 1.0;
      }

    });
  }

  makeGraphRealTime() {
    return SfCartesianChart(
        zoomPanBehavior:
            ZoomPanBehavior(enablePanning: true, enablePinching: true),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: '',
                    fontSize: 16,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: '',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 20,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'Real-Time Graph',
            textStyle: ChartTextStyle(
                fontFamily: 'Prompt',
                color: Colors.blueGrey,
                fontSize: 18,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend
        legend: Legend(isVisible: true, position: LegendPosition.bottom),

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataGauge, String>>[
          SplineSeries<GraphDataGauge, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "PM2.5",
            dataSource: linePM25,
            xValueMapper: (GraphDataGauge graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataGauge graph, _) => graph.y,
            color: Color.fromRGBO(192, 108, 132, 1),
          ),
          SplineSeries<GraphDataGauge, String>(
            markerSettings: MarkerSettings(
                isVisible: true,
                // Marker shape is set to diamond
                shape: DataMarkerType.circle),
            isVisible: true,
            animationDuration: 0.5,
            width: 2,
            name: "PM10",
            dataSource: linePM10,
            xValueMapper: (GraphDataGauge graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataGauge graph, _) => graph.y,
            color: Color.fromRGBO(75, 135, 185, 1),
          ),
        ]);
  }

  @override
  void initState() {


    getLast();

    _mqttUtility = new MQTTUtility(
        (onMessageNew, topic) => getMessage(onMessageNew, topic));
    Future<bool> status = _mqttUtility.connect();

    print(status);
    status.then((value) => {
          if (value)
            {
              _mqttUtility.subscribeToTopic("${widget.uid}"),
            }
          else
            {print(value)}
        });

    imageName = asset[state];
  }

  addDataPM25(double d) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('HH:mm:ss');
    datetime = formatter.format(now);
    setState(() {
      if (linePM25.length >= 10) {
        linePM25.removeAt(0);
      }
      linePM25.add(GraphDataGauge("${datetime}", d / 1.0));
    });
  }

  addDataPM10(double d) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('HH:mm:ss');
    datetime = formatter.format(now);
    setState(() {
      if (linePM10.length >= 10) {
        linePM10.removeAt(0);
      }
      linePM10.add(GraphDataGauge("${datetime}", d / 1.0));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
            backgroundColor: LightColors.kGreen,
            title: Text(
              widget.name,
              style: TextStyle(fontFamily: 'Prompt'),
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.equalizer),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (_) => DetailPage(
                            uid: widget.uid,
                            value: aqi,
                            image: imageName,
                          )));
                },
              ),
            ]),
        backgroundColor: LightColors.kLightGreen,
        body: SingleChildScrollView(
            child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 10),
                    GestureDetector(
                        child: CircleAvatar(
                            maxRadius: 100,
                            backgroundImage: AssetImage(imageName))),
                    SizedBox(height: 10),
                    Container(
                      child: Text("AQI : ${aqi}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                              fontFamily: 'Prompt')),
                    )
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 10),
                    Container(
                      margin: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10),
                      child: Text("Real-Time ${datetime}",
                          style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 25,
                              fontFamily: 'Prompt')),
                    )
                  ],
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 5, 0, 5),
            ),

            Container(
                margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                    elevation: 5,
                    child: Container(child: makeGraphRealTime()))),

            Container(
                margin: EdgeInsets.all(20),
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 2,
                  child: Container(
                      child: SfRadialGauge(
                          title: GaugeTitle(
                              text: 'PM2.5 : ${pm25} ug/m3',
                              textStyle: TextStyle(
                                  fontSize: 30,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'Prompt')),
                          axes: <RadialAxis>[
                        RadialAxis(
                            axisLabelStyle: GaugeTextStyle(
                                fontSize: 20,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Prompt'),
                            minimum: 0,
                            maximum: 50,
                            ranges: <GaugeRange>[
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 0,
                                  endValue: 25,
                                  color: Colors.blue),
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 25,
                                  endValue: 37,
                                  color: Colors.green),
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 37,
                                  endValue: 50,
                                  color: Colors.yellow),
                            ],
                            pointers: <GaugePointer>[
                              NeedlePointer(
                                  value: pm25 / 1.0,
                                  lengthUnit: GaugeSizeUnit.factor,
                                  needleLength: 0.8,
                                  needleEndWidth: 10,
                                  gradient: const LinearGradient(
                                      colors: <Color>[
                                        darkGrey,
                                        darkGrey,
                                        Colors.black,
                                        Colors.black
                                      ],
                                      stops: <double>[
                                        0,
                                        0.5,
                                        0.5,
                                        1
                                      ]),
                                  needleColor: const Color(0xFFF67280),
                                  knobStyle: KnobStyle(
                                      knobRadius: 0.08,
                                      sizeUnit: GaugeSizeUnit.factor,
                                      color: Colors.black))
                            ],
                            annotations: <GaugeAnnotation>[
                              GaugeAnnotation(angle: 90, positionFactor: 0.5)
                            ])
                      ])),
                )),

            Container(
                margin: EdgeInsets.all(20),
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  elevation: 2,
                  child: Container(
                      child: SfRadialGauge(
                          title: GaugeTitle(
                              text: 'PM10 : ${pm10} ug/m3',
                              textStyle: TextStyle(
                                  fontSize: 30,
                                  fontStyle: FontStyle.normal,
                                  fontFamily: 'Prompt')),
                          axes: <RadialAxis>[
                        RadialAxis(
                            axisLabelStyle: GaugeTextStyle(
                                fontSize: 20,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'Prompt'),
                            minimum: 0,
                            maximum: 50,
                            ranges: <GaugeRange>[
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 0,
                                  endValue: 25,
                                  color: Colors.blue),
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 25,
                                  endValue: 37,
                                  color: Colors.green),
                              GaugeRange(
                                  startWidth: 20,
                                  endWidth: 20,
                                  startValue: 37,
                                  endValue: 50,
                                  color: Colors.yellow),
                            ],
                            pointers: <GaugePointer>[
                              NeedlePointer(
                                  value: pm10 / 1.0,
                                  lengthUnit: GaugeSizeUnit.factor,
                                  needleLength: 0.8,
                                  needleEndWidth: 10,
                                  gradient: const LinearGradient(
                                      colors: <Color>[
                                        darkGrey,
                                        darkGrey,
                                        Colors.black,
                                        Colors.black
                                      ],
                                      stops: <double>[
                                        0,
                                        0.5,
                                        0.5,
                                        1
                                      ]),
                                  needleColor: const Color(0xFFF67280),
                                  knobStyle: KnobStyle(
                                      knobRadius: 0.08,
                                      sizeUnit: GaugeSizeUnit.factor,
                                      color: Colors.black))
                            ],
                            annotations: <GaugeAnnotation>[
                              GaugeAnnotation(angle: 90, positionFactor: 0.5)
                            ])
                      ])),
                )),
          ],
        )));

  }
}
     