import 'dart:async';
import 'dart:convert';
import 'package:aqi/admin_page.dart';
import 'package:aqi/config.dart';
import 'package:aqi/gauge.dart';
import 'package:aqi/model/machine.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MapSample extends StatefulWidget {
  @override
  State<MapSample> createState() => MapSampleState();
}

class MapSampleState extends State<MapSample> with TickerProviderStateMixin {
  Completer<GoogleMapController> _controller = Completer();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Machine machine;

  Set<Marker> _markers = {};

  int color = 0;
int state = 0;
  List<Color> colorArea = [
    Colors.blueAccent,
    Colors.green,
    Colors.yellow,
    Colors.deepOrange,
    Colors.red
  ];
  List<int> colorValue = [20, 44, 66, 135, 205];

  void callMap() async {
    _markers = {};

    Response response =
        await get("${Config.urlHTTP}/machine", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());


    print("jsonResponse ${jsonResponse.toString()}");
    setState(() {
      machine = Machine.fromJson(jsonResponse);

      for (int i = 0; i < machine.message.data.length; i++) {
        int values = colorValue[color];
        var _marker = Marker(
            onTap: () => {

              print(machine.message.data[i].uid),
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (_) => GaugePage(uid: machine.message.data[i].uid, value: values, name: "${machine.message.data[i].name}",)))
                },
            infoWindow: InfoWindow(
                title: "",
                snippet:
                    "${machine.message.data[i].name}"),
            position: LatLng(
                machine.message.data[i].lat, machine.message.data[i].lng),
            markerId: MarkerId("${i}"));

        _markers.add(_marker);

        color = 0;
      }
    });
  }

  @override
  void initState() {

    getStateUser();
    callMap();
  }

  void getStateUser() async{
    final SharedPreferences prefs = await _prefs;

    setState(() {
      state = prefs.getInt("user");
    });

  }

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(6.751659, 100.324862),
    zoom: 7,
  );

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () {
              callMap();
            },
          ),
          (state == 1) ?
          // action button
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => AdminPage()));
            },
          ) : Container(),
        ],
        backgroundColor: LightColors.kGreen,
        title: Text("Map", style: TextStyle(fontFamily: 'Prompt')),
      ),
      body: GoogleMap(
        markers: _markers,
        mapType: MapType.satellite,
        initialCameraPosition: _kGooglePlex,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }
}
