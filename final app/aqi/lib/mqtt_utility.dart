import 'dart:async';

import 'package:mqtt_client/mqtt_client.dart' as mqtt;

class MQTTUtility{

  String broker           = '35.240.153.210';
  int port                = 1883;
  String username         = 'win';
  String passwd           = '@#Win2020';
  String clientIdentifier = 'WINAP${new DateTime.now().millisecondsSinceEpoch}';

  mqtt.MqttClient client;
  mqtt.MqttConnectionState connectionState;
  StreamSubscription subscription;

  final Function(String , String) onMessageNew;

  MQTTUtility(this.onMessageNew);
  String message;
  Future<bool> connect() async {
    client = mqtt.MqttClient(broker, '');
    client.port = port;
    client.logging(on: true);
    client.keepAlivePeriod = 30;

    final mqtt.MqttConnectMessage connMess = mqtt.MqttConnectMessage()

        .withClientIdentifier(clientIdentifier)
        .startClean()
        .keepAliveFor(30)
        .withWillQos(mqtt.MqttQos.atMostOnce);
    client.connectionMessage = connMess;

    try {
      await client.connect(username, passwd);

      subscription = client.updates.listen(_onMessage);
      return true;
    } catch (e) {

      print("error");
      print(e);
      disconnect();

      return false;
    }
  }
  Future<void> disconnect() async {
    client.disconnect();
  }

  void subscribeToTopic(String topic) {
    client.subscribe(topic, mqtt.MqttQos.exactlyOnce);
  }

  void _onMessage(List<mqtt.MqttReceivedMessage> event) async {
    final mqtt.MqttPublishMessage recMess =
    event[0].payload as mqtt.MqttPublishMessage;
    message = mqtt.MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

    String topic = event[0].topic;
    onMessageNew(message , topic);
  }


}