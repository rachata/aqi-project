import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:aqi/config.dart';
import 'package:aqi/model/uid.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:loading/indicator/ball_pulse_indicator.dart';
import 'package:loading/loading.dart';
import 'package:progress_dialog/progress_dialog.dart';

class SetiingPage extends StatefulWidget {
  SetiingPage({Key key}) : super(key: key);

  @override
  SetiingPageState createState() => SetiingPageState();
}

enum ConfirmAction { CANCEL, ACCEPT }

class SetiingPageState extends State<SetiingPage> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(7.205948, 100.590471),
    zoom: 8,
  );

  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Set<Polygon> _polygon = {};
  List<Marker> listMarker = [];
  List<LatLng> listLatLng = [];
  GoogleMap google;

  Set<Marker> finalArea = {};
  bool statusPin = false;

  var devices = ["ยังไม่มี Device"];

  String deviceSelect = "";
  LatLng area;
  TextEditingController position =
      TextEditingController(text: '7.205948,100.590471');

  TextEditingController line = TextEditingController(text : '');

  TextEditingController name = TextEditingController();
  static int map = 1;

  static List<MapType> MapTypes = [
    MapType.none,
    MapType.normal,
    MapType.satellite,
    MapType.terrain,
    MapType.hybrid
  ];

  MapType type = MapTypes[map];
  Uint8List markerIcon;

  LatLng main;

  int areaNumber = 1;

  ProgressDialog pr;

  @override
  void initState() {

    callUID();
    deviceSelect = devices[0];
    setCustomMapPin();

    pr = ProgressDialog(
      context,
      type: ProgressDialogType.Download,
      textDirection: TextDirection.ltr,
      isDismissible: false,
    );

    pr.style(
        message: "Please wait...",
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        insetAnimCurve: Curves.easeInOut,
        progressTextStyle: TextStyle(fontFamily: 'Prompt',
            color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(fontFamily: 'Prompt',
            color: Colors.black, fontSize: 19.0, fontWeight: FontWeight.w600));

  }

  void drawPolygon() {
    print(listLatLng.toString());
    setState(() {
      _polygon = {};
    });
    if (listLatLng.length >= 3) {
      List<LatLng> list = List();
      for (int i = 0; i < listLatLng.length; i++) {
        list.add(LatLng(listLatLng[i].latitude, listLatLng[i].longitude));
      }

      list.add(list[0]);

      var poly = Polygon(
          onTap: () {},
          polygonId: PolygonId("area1"),
          strokeWidth: 2,
          strokeColor: LightColors.kDarkYellow,
          fillColor: Colors.yellow.withOpacity(0.6),
          points: list);

      setState(() {
        _polygon.add(poly);
      });
    } else {
      _showMyDialog("ไม่สามารถวาดได้ เนื่องจากจุดน้อยกว่า 3 จุด");
    }
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 0.5), 'assets/marker64.png');

    setState(() {
      statusPin = true;

      main = LatLng(7.205948, 100.590471);
      var marker = Marker(
          infoWindow: InfoWindow(title: "", snippet: "เครื่องวัด"),
          onTap: () {
            print('Tapped');
          },
          draggable: true,
          markerId: MarkerId('0'),
          position: LatLng(7.205948, 100.590471),
          onDragEnd: ((value) {
            setState(() {
              print("setCustomMapPin ${value.toString()}");
              main = value;

              print("mains ${main}");
              position.text = "${value.latitude},${value.longitude}";
            });
          }));
      _markers.add(marker);

      listMarker.add(marker);
    });
  }

  Future<void> _showMyDialog(String text) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text('Setting' , style: TextStyle(fontFamily: 'Prompt',),),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("${text}" ,  style: TextStyle(fontSize: 15, fontFamily: 'Prompt',)),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('ตกลง' ,  style: TextStyle(fontFamily: 'Prompt',)),
              onPressed: () {
                Navigator.of(context).pop();
               Navigator.of(context).pop();
//                Navigator.of(context)
//                    .pushReplacement(MaterialPageRoute(builder: (_) => LoginPage()));
              },
            ),
          ],
        );
      },
    );
  }

  Widget getButton() {
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16, top: 8),
      child: Container(
        height: 48,
        margin: new EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: LightColors.kBlue.withOpacity(0.9),
          borderRadius: const BorderRadius.all(Radius.circular(24.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey.withOpacity(0.4),
              blurRadius: 8,
              offset: const Offset(4, 4),
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: const BorderRadius.all(Radius.circular(24.0)),
            highlightColor: Colors.transparent,
            onTap: () {
              callAPI();
            },
            child: Center(
              child: Text(
                'บันทึก',
                style: TextStyle(
                    fontFamily: 'Prompt',
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }

  callUID() async{

    Response response = await get("${Config.urlHTTP}/uid/empty",
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponse = json.decode(response.body.toString());

    print(jsonResponse.toString());

    setState(() {

      UID  uid = UID.fromJson(jsonResponse);

      if(uid.status == 200){

        for(int i = 0; i < uid.message.length; i++){
            devices.add("${uid.message[i].note}:${uid.message[i].text}");
        }
      }
    });

  }
  callAPI() async {
    await pr.show();
    print(" 0 0 0 0 00 0 ");
    print(main.toString());
    print(listLatLng.toString());

    var arr = [];

    if (listLatLng.length >= 1) {
      for (int i = 0; i < listLatLng.length; i++) {
        arr.add({
          "lat": listLatLng[i].latitude,
          "lng": listLatLng[i].longitude,
        });
      }
    }
    String nameMachine = name.text.toString();

    String uid = "";
    if(deviceSelect == "ยังไม่มี Device"){
      uid = "";
    }else{

      uid = deviceSelect.split(":")[1];
    }
    if (nameMachine != "") {
      final jsonsp = jsonEncode({
        "name": nameMachine,
        "lat": main.latitude,
        "lng": main.longitude,
        "uid": uid,
        "line": line.text.toString(),
        "area": arr
      });

      print(json.decode.toString());
      Response response = await post("${Config.urlHTTP}/machine",
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonsp);

      final jsonResponse = json.decode(response.body.toString());

      print(jsonResponse.toString());

      if (jsonResponse['status'] == 200) {
        pr.hide();
        removePinArea();

        _showMyDialog('เพิ่มสำเร็จ');
      }
    } else {
      pr.hide();
      _showMyDialog("โปรดป้อนชื่อสถานที่");
    }
    pr.hide();
  }

  void typeMap() {
    setState(() {
      if (map == 4) {
        map = 0;
      } else {
        map++;
      }
      type = MapTypes[map];
    });
  }

  void setMarkLocation() async {
    print("setMarkLocation");
    Position positions = await getCurrentPosition(
        desiredAccuracy: LocationAccuracy.bestForNavigation);

    setState(() {
      main = LatLng(positions.latitude, positions.longitude);

      var marker = Marker(
          infoWindow: InfoWindow(title: "", snippet: "เครื่องวัด"),
          markerId: MarkerId('0'),
          draggable: true,
          position: main,
          onDragEnd: ((value) {
            setState(() {
              main = value;
              print("mains ${main}");
              position.text = "${value.latitude},${value.longitude}";
            });
          }));

      _markers.remove(_markers.first);
      _markers.add(marker);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          // action button
          IconButton(
            icon: Icon(Icons.map),
            onPressed: () {
              typeMap();
            },
          ),

          IconButton(
            icon: Icon(Icons.crop_free),
            onPressed: () {
              setMarkLocation();
            },
          ),

          IconButton(
            icon: Icon(Icons.panorama_horizontal),
            onPressed: () {
              drawPolygon();
            },
          ),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => {
              _showMyDialogs().then((value) => {
                    if (value == ConfirmAction.ACCEPT) {addArea()}
                  })
            },
          ),
          IconButton(
            icon: Icon(Icons.remove_circle),
            onPressed: () {
              removePinArea();
//              typeMap();
            },
          ),

//          IconButton(
//            icon: Icon(Icons.add_box),
//            onPressed: () {
////              typeMap();
//              addPinArea();
//            },
//          ),
        ],
        backgroundColor: LightColors.kGreen,
        title: Text("Setting Data" ,style: TextStyle(  fontFamily: 'Prompt',),),
      ),
      backgroundColor: LightColors.kLightYellow,
      body: (statusPin == false)
          ? Container(
              child: Center(
                child: Loading(
                    indicator: BallPulseIndicator(),
                    size: 100.0,
                    color: Colors.red),
              ),
            )
          : SafeArea(
              top: true,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 10),
                  child: Column(
                    children: <Widget>[
                      Container(
                          child: SizedBox(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                  left: 10.0,
                                  top: 4.0,
                                  bottom: 4.0,
                                  right: 10.0),
                              padding: EdgeInsets.only(
                                  left: 16.0, top: 4.0, bottom: 4.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(10.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.6),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 9.0),
                                ],
                              ),
                              child: TextField(
                                style: TextStyle(fontFamily: 'Prompt',),
                                controller: name,
                                decoration: InputDecoration(
                                    labelText: "ชื่อสถานที่",
                                    labelStyle: TextStyle(  fontFamily: 'Prompt',),
                                    border: InputBorder.none),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 10.0,
                                  top: 4.0,
                                  bottom: 4.0,
                                  right: 10.0),
                              padding: EdgeInsets.only(
                                  left: 16.0, top: 4.0, bottom: 4.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(10.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.6),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 9.0),
                                ],
                              ),
                              child: TextField(
                                style: TextStyle(fontFamily: 'Prompt',),
                                controller: position,
                                decoration: InputDecoration(
                                    labelText: "ตำแหน่งเครื่องวัด",
                                    labelStyle: TextStyle(  fontFamily: 'Prompt',),
                                    border: InputBorder.none),
                              ),
                            ),


                            Container(
                              margin: EdgeInsets.only(
                                  left: 10.0,
                                  top: 4.0,
                                  bottom: 4.0,
                                  right: 10.0),
                              padding: EdgeInsets.only(
                                  left: 16.0, top: 4.0, bottom: 4.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(10.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.6),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 9.0),
                                ],
                              ),
                              child: DropdownButton<String>(

                                hint: Text("Device"),
                                isExpanded: true,
                                items: devices.map((String value) {
                                  return new DropdownMenuItem<String>(
                                    value: value,
                                    child: new Text(value),
                                  );
                                }).toList(),
                                onChanged: (newValue) {
                                  setState(
                                          () => {deviceSelect = newValue});
                                },
                                value: deviceSelect,
                              ),
                            ),

                            Container(
                              margin: EdgeInsets.only(
                                  left: 10.0,
                                  top: 4.0,
                                  bottom: 4.0,
                                  right: 10.0),
                              padding: EdgeInsets.only(
                                  left: 16.0, top: 4.0, bottom: 4.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(10.0)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.6),
                                      offset: const Offset(1.1, 1.1),
                                      blurRadius: 9.0),
                                ],
                              ),
                              child: TextField(
                                controller: line,
                                style: TextStyle(fontFamily: 'Prompt',),
                                keyboardType: TextInputType.multiline,
                                decoration: InputDecoration(

                                    labelText: "Line Token",
                                    labelStyle: TextStyle(  fontFamily: 'Prompt',),
                                    border: InputBorder.none),
                              ),
                            ),

                            Container(

                                height: 300,
                                child: GoogleMap(
                                  polygons: _polygon,
                                  markers: _markers,
                                  mapType: type,
                                  initialCameraPosition: _kGooglePlex,
                                  onMapCreated:
                                      (GoogleMapController controller) {
                                    _controller.complete(controller);
                                  },
                                )),
                            Container(

//                              margin: EdgeInsets.only(
//                                  left: 10.0,
//                                  top: 4.0,
//                                  bottom: 4.0,
//                                  right: 10.0),
//                              padding: EdgeInsets.only(
//                                  left: 16.0, top: 4.0, bottom: 4.0),
                              child: getButton(),
                            )
                          ],
                        ),
                      )),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  removePinArea() {
    setState(() {
      finalArea.forEach((element) {
        _markers.remove(element);
      });

      listLatLng = [];
      _polygon = {};
    });
  }

  Future<ConfirmAction> _showMyDialogs() async {
    var mainMarker = Marker(
      infoWindow: InfoWindow(title: "", snippet: "เครื่องวัด"),
      markerId: MarkerId('m1'),
      position: LatLng(main.latitude, main.longitude),
    );

    var marker = Marker(
        infoWindow: InfoWindow(title: "", snippet: "Area ${areaNumber}"),
        icon: pinLocationIcon,
        draggable: true,
        markerId: MarkerId('A${areaNumber}'),
        position: LatLng(7.205948, 100.590471),
        onDragEnd: ((value) {
          setState(() {
            area = value;

            print(area.toString());
          });
        }));

    Set<Marker> markerSet = {};
    markerSet.add(marker);
    markerSet.add(mainMarker);

    return showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6),
      barrierDismissible: false,
      barrierLabel: "Dialog",
      transitionDuration: Duration(milliseconds: 400),
      pageBuilder: (_, __, ___) {
        return SizedBox.expand(
          child: Column(
            children: <Widget>[
              Expanded(
                  flex: 5,
                  child: GoogleMap(
                    markers: markerSet,
                    mapType: type,
                    initialCameraPosition: _kGooglePlex,
                    onMapCreated: (GoogleMapController controller) {
                      _controller.complete(controller);
                    },
                  )),
              Expanded(
                flex: 1,
                child: SizedBox.expand(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    RaisedButton(
                        color: LightColors.kGreen,
                        child: Text(
                          "สร้าง",

                          style: TextStyle(fontSize: 20 ,  fontFamily: 'Prompt',),
                        ),
                        textColor: Colors.white,
                        onPressed: () =>
                            Navigator.of(context).pop(ConfirmAction.ACCEPT)),
                    RaisedButton(
                        color: LightColors.kRed,
                        child: Text(
                          "ยกเลิก",
                          style: TextStyle(fontSize: 20 , fontFamily: 'Prompt'),
                        ),
                        textColor: Colors.white,
                        onPressed: () =>
                            Navigator.of(context).pop(ConfirmAction.CANCEL)),
                  ],
                )),
              ),
            ],
          ),
        );
      },
    );
  }

  void addArea() {
    var marker = Marker(
      infoWindow: InfoWindow(title: "", snippet: "Area ${areaNumber}"),
      onTap: () {
        print('Tapped');
      },
      icon: pinLocationIcon,
      markerId: MarkerId('F${areaNumber}'),
      position: area,
    );

    setState(() {
      areaNumber = areaNumber + 1;
      finalArea.add(marker);

      listLatLng.add(area);
      _markers.add(marker);
    });

    print(listLatLng.toString());
    print(" print(listMarker.toString()); ${listLatLng.length}");
  }
}
