import 'dart:convert';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:aqi/config.dart';
import 'package:aqi/model/year.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_pickers/flutter_material_pickers.dart';
import 'package:http/http.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class DetailPage extends StatefulWidget {
  final String image;
  final String value;
  final String uid;


  const DetailPage({Key key, this.uid ,  this.image, this.value}) : super(key: key);

  @override
  State<DetailPage> createState() => DetailPageState();
}

class DetailPageState extends State<DetailPage> {
  List<GraphDataLog> logPM25 = [];
  List<GraphDataLog> logPM10 = [];
  List<GraphDataLog> logSO2 = [];
  List<GraphDataLog> logO3 = [];
  List<GraphDataLog> logCO = [];
  List<GraphDataLog> logNO2 = [];

  int _value = 1;

  var selectYear = "";
  List<String> years = <String>[];

  String start = "";
  String end = "";

  @override
  void initState() {
    getYear();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          backgroundColor: LightColors.kGreen,
          title: Text(
            "Graph",
            style: TextStyle(fontFamily: 'Prompt'),
          ),
        ),
        backgroundColor: LightColors.kLightGreen,
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: <
                  Widget>[
                Column(children: <Widget>[
                  GestureDetector(
                    child: CircleAvatar(
                        maxRadius: 48,
                        backgroundImage: AssetImage(widget.image)),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'AQI : ${widget.value}',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontFamily: 'Prompt'),
                    ),
                  ),
                ]),
                ChoiceChip(
                  label: Text(
                    "วัน",
                    style: TextStyle(color: Colors.white, fontFamily: 'Prompt'),
                  ),
                  selected: _value == 1,
                  selectedColor: LightColors.kRed,
                  onSelected: (bool value) {
                    callDatePicker();
                    setState(() {
                      _value = value ? 1 : null;
                    });
                  },
                  backgroundColor: LightColors.kBlue,
                  labelStyle: TextStyle(color: Colors.white),
                ),
                ChoiceChip(
                  label: Text("เดือน",
                      style:
                          TextStyle(color: Colors.white, fontFamily: 'Prompt')),
                  selected: _value == 2,
                  selectedColor: LightColors.kRed,
                  onSelected: (bool value) {
                    callMonthPicker();
                    setState(() {
                      _value = value ? 2 : null;
                    });
                  },
                  backgroundColor: LightColors.kBlue,
                  labelStyle: TextStyle(color: Colors.white),
                ),
                ChoiceChip(
                  label: Text("ปี",
                      style:
                          TextStyle(color: Colors.white, fontFamily: 'Prompt')),
                  selected: _value == 3,
                  selectedColor: LightColors.kRed,
                  onSelected: (bool value) {
                    callYear();
                    setState(() {
                      _value = value ? 3 : null;
                    });
                  },
                  backgroundColor: LightColors.kBlue,
                  labelStyle: TextStyle(color: Colors.white),
                ),
                ChoiceChip(
                  label: Text("ขั้นสูง",
                      style:
                          TextStyle(color: Colors.white, fontFamily: 'Prompt')),
                  selected: _value == 3,
                  selectedColor: LightColors.kRed,
                  onSelected: (bool value) {
                    callAdvanced();
                    setState(() {
                      _value = value ? 4 : null;
                    });
                  },
                  backgroundColor: LightColors.kBlue,
                  labelStyle: TextStyle(color: Colors.white),
                ),
              ]),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumn()))),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumnPM10()))),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumnCO()))),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumnNO()))),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumnSO()))),
              Container(
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                  child: Card(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                      elevation: 5,
                      child: Container(child: makeGraphColumnO3()))),
            ],
          ),
        ));
  }

  makeGraphColumn() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelStyle: TextStyle(fontFamily: 'Prompt'),
            labelFormat: '{value}',
            title: AxisTitle(
                text: 'ug/m3',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            labelStyle: TextStyle(fontFamily: 'Prompt'),
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า PM2.5',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontFamily: 'Prompt',
                fontSize: 12,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend
        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "PM2.5",
            dataSource: logPM25,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: LightColors.kRed,
            width: 0.7,
          ),
        ]);
  }

  makeGraphColumnPM10() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            labelStyle: TextStyle(
              fontFamily: 'Prompt',
            ),
            title: AxisTitle(
                text: 'ug/m3',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า PM10',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontSize: 12,
                fontFamily: 'Prompt',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "PM10",
            dataSource: logPM10,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: LightColors.kDarkBlue,
            width: 0.7,
          ),
        ]);
  }

  makeGraphColumnCO() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelStyle: TextStyle(
              fontFamily: 'Prompt',
            ),
            labelFormat: '{value}',
            title: AxisTitle(
                text: 'PPM',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า CO',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontSize: 12,
                fontFamily: 'Prompt',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "CO",
            dataSource: logCO,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: Colors.brown,
            width: 0.7,
          ),
        ]);
  }

  makeGraphColumnNO() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            labelStyle: TextStyle(
              fontFamily: 'Prompt',
            ),
            title: AxisTitle(
                text: 'PPM',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า NO2',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontSize: 12,
                fontFamily: 'Prompt',
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "NO2",
            dataSource: logNO2,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: Colors.red,
            width: 0.7,
          ),
        ]);
  }

  makeGraphColumnSO() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            labelStyle: TextStyle(
              fontFamily: 'Prompt',
            ),
            title: AxisTitle(
                text: 'PPM',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า SO2',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontFamily: 'Prompt',
                fontSize: 12,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "SO2",
            dataSource: logSO2,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: Colors.green,
            width: 0.7,
          ),
        ]);
  }

  makeGraphColumnO3() {
    return SfCartesianChart(
        zoomPanBehavior: ZoomPanBehavior(
            enablePanning: true,
            enableDoubleTapZooming: true,
            enablePinching: true,
            enableSelectionZooming: true,
            enableMouseWheelZooming: true),
        legend: Legend(isVisible: true, position: LegendPosition.bottom),
        plotAreaBackgroundColor: Colors.white70,
        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            labelStyle: TextStyle(
              fontFamily: 'Prompt',
            ),
            title: AxisTitle(
                text: 'PPM',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            labelRotation: 60,
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: 'Prompt',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงค่า O3',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontFamily: 'Prompt',
                fontSize: 12,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend

        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphDataLog, String>>[
          ColumnSeries<GraphDataLog, String>(
            name: "O3",
            dataSource: logO3,
            xValueMapper: (GraphDataLog graph, _) => graph.x.toString(),
            yValueMapper: (GraphDataLog graph, _) => graph.y,
            color: Colors.indigo,
            width: 0.7,
          ),
        ]);
  }

  void callMonthPicker() {
    showMonthPicker(
      context: context,
      initialDate: DateTime.now(),
      locale: Locale("th"),
    ).then((date) {
      if (date != null) {
        setState(() {
          print("date ${date}");

          final DateFormat formatter = DateFormat('yyyy-MM-dd');
          String m = formatter.format(date);

          callGetMonth(m);
        });
      }
    });
  }

  void callYear() {
    showMaterialScrollPicker(
      context: context,
      title: "เลือกปีที่ต้องการ",
      items: years,
      selectedItem: selectYear,
      onChanged: (value) => {callGetYear(value)},
    );
  }

  void getYear() async {
    Response response =
        await get("${Config.urlHTTP}/log/year", headers: <String, String>{
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json; charset=UTF-8',
    });

    final jsonResponse = json.decode(response.body.toString());
    Year year = Year.fromJson(jsonResponse);

    if (year.status == 200) {
      setState(() {
        for (int i = 0; i < year.message.length; i++) {
          years.add(year.message[i].dtf);
        }
      });
    }
  }

  void callGetYear(String data) async {
    Response response = await get("${Config.urlHTTP}/log/year/${data}/${widget.uid}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      if (jsonResponse['message'].length != 0) {
        logPM25 = [];
        logPM10 = [];
        logSO2 = [];
        logO3 = [];
        logCO = [];
        logNO2 = [];
      } else {
        _showMyDialog();
      }
      for (int i = 0; i < jsonResponse['message'].length; i++) {
        setState(() {
          logPM25.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm25'] / 1.0));
          logPM10.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm10'] / 1.0));
          logSO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['so2'] / 1.0));
          logO3.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['o3'] / 1.0));
          logCO.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['co'] / 1.0));
          logNO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['no2'] / 1.0));
        });
        print(logO3.length);
        print(logSO2.length);
        print(logCO.length);
        print(logNO2.length);
      }
    }
  }

  void callGetMonth(String data) async {
    print("${Config.urlHTTP}/log/month/${data}");
    Response response = await get("${Config.urlHTTP}/log/month/${data}/${widget.uid}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    print(response.statusCode);
    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      if (jsonResponse['message'].length != 0) {
        logPM25 = [];
        logPM10 = [];
        logSO2 = [];
        logO3 = [];
        logCO = [];
        logNO2 = [];
      } else {
        _showMyDialog();
      }
      for (int i = 0; i < jsonResponse['message'].length; i++) {
        setState(() {
          logPM25.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm25'] / 1.0));
          logPM10.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm10'] / 1.0));
          logSO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['so2'] / 1.0));
          logO3.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['o3'] / 1.0));
          logCO.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['co'] / 1.0));
          logNO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['no2'] / 1.0));
        });
      }
    }
  }

  void callGetDay(String data) async {
    Response response = await get("${Config.urlHTTP}/log/day/${data}/${widget.uid}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    print(response.statusCode);
    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      if (jsonResponse['message'].length != 0) {
        logPM25 = [];
        logPM10 = [];
        logSO2 = [];
        logO3 = [];
        logCO = [];
        logNO2 = [];
      } else {
        _showMyDialog();
      }
      for (int i = 0; i < jsonResponse['message'].length; i++) {
        setState(() {
          logPM25.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm25'] / 1.0));
          logPM10.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm10'] / 1.0));
          logSO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['so2'] / 1.0));
          logO3.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['o3'] / 1.0));
          logCO.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['co'] / 1.0));
          logNO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['no2'] / 1.0));
        });
      }
    }
  }

  void callDatePicker() async {
    var order = await getDate();
    setState(() {
      print(order.toIso8601String());

      final DateFormat formatter = DateFormat('yyyy-MM-dd');
      String m = formatter.format(order);
      callGetDay(m);
    });
  }

  void callAdvanced() {
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');

    DatePicker.showDateTimePicker(context,
        showTitleActions: true, maxTime: new DateTime.now(), onChanged: (date) {
      print('change $date');
    }, onConfirm: (date) {
      start = formatter.format(date);

      DatePicker.showDateTimePicker(context,
          showTitleActions: true,
          maxTime: new DateTime.now(), onChanged: (date) {
        print('change $date');
      }, onConfirm: (date) {
        end = formatter.format(date);

        if (start != "" && end != "") {
          callGetAdvanced(start, end);
        }
      }, currentTime: DateTime.now(), locale: LocaleType.th);
    }, currentTime: DateTime.now(), locale: LocaleType.th);
  }

  void callGetAdvanced(String start, String end) async {
    Response response = await get(
        "${Config.urlHTTP}/log/advanced/${start}/${end}/${widget.uid}",
        headers: <String, String>{
          'Accept': 'application/json; charset=UTF-8',
          'Content-Type': 'application/json; charset=UTF-8',
        });

    print(response.statusCode);
    final jsonResponse = json.decode(response.body.toString());

    if (jsonResponse['status'] == 200) {
      if (jsonResponse['message'].length != 0) {
        logPM25 = [];
        logPM10 = [];
        logSO2 = [];
        logO3 = [];
        logCO = [];
        logNO2 = [];
      } else {
        _showMyDialog();
      }
      for (int i = 0; i < jsonResponse['message'].length; i++) {
        setState(() {
          logPM25.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm25'] / 1.0));
          logPM10.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['pm10'] / 1.0));
          logSO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['so2'] / 1.0));
          logO3.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['o3'] / 1.0));
          logCO.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['co'] / 1.0));
          logNO2.add(GraphDataLog(jsonResponse['message'][i]['dtf'],
              jsonResponse['message'][i]['no2'] / 1.0));
        });
        print(logO3.length);
        print(logSO2.length);
        print(logCO.length);
        print(logNO2.length);
      }
    }
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Text(
            'Graph',
            style: TextStyle(
              fontFamily: 'Prompt',
            ),
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(
                  "ไม่พบข้อมูล",
                  style: TextStyle(fontFamily: 'Prompt', fontSize: 16),
                ),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'ตกลง',
                style: TextStyle(
                  fontFamily: 'Prompt',
                ),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future<DateTime> getDate() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: LightColors.kDarkBlue,
          ),
          child: child,
        );
      },
    );
  }

  Future<DateTime> getDateStart() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(1900),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: Theme.of(context).copyWith(
            primaryColor: LightColors.kDarkBlue,
          ),
          child: child,
        );
      },
    );
  }
}

class GraphDataLog {
  GraphDataLog(this.x, this.y);

  final String x;
  final double y;
}
