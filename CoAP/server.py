from coapthon.server.coap import CoAP
from coapthon import defines
from coapthon.resources.resource import Resource
import json
from datetime import datetime
import requests
import ast


import paho.mqtt.publish as publish
import time

hostname = "35.247.128.116"
port = 1883
auth = {
 'username':'appkin',
 'password':'@#Appkin3122'
}


def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    client.subscribe("$SYS/#")

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))



class AdvancedResource(Resource):
    def __init__(self, name="Advanced"):
        super(AdvancedResource, self).__init__(name)
        self.payload = "Advanced resource"

    def render_GET_advanced(self, request, response):
        response.payload = self.payload
        response.max_age = 20
        response.code = defines.Codes.CONTENT.number
        return self, response

    def render_POST_advanced(self, request, response):

        from coapthon.messages.response import Response
        assert(isinstance(response, Response))
	print(str(request.payload))
	jsonTopic = json.loads(json.dumps(str(request.payload)))
        jsonCoAP = json.loads(json.dumps(str(request.payload)))   
        jsonCoAP = ast.literal_eval(jsonCoAP)
	topic = (str(jsonCoAP["uid"]))
        jsonCoAP = json.dumps(jsonCoAP) 
	print("topiv "+topic)
        data = {
            "log" : jsonCoAP
        }
        print(data);

# MQTT
        publish.single("win/all",       str(jsonCoAP), hostname=hostname, port=port, auth=auth)
#	print(str(jsonTopic['uid']))
        publish.single(topic ,     str(jsonCoAP), hostname=hostname, port=port, auth=auth)

# MQTT



# HTTP

        responsesHttp = requests.post("http://35.240.153.210:8989/log", data=data)
        print(responsesHttp.status_code);
        print(responsesHttp.text);

# HTTP


        responseHTTP = json.loads(responsesHttp.text)
        response.code = defines.Codes.CREATED.number
        response.payload = (defines.Content_types["application/json"],json.dumps(responseHTTP))
   # 	response.payload  = "TEST 01"    


        return self, response


class CoAPServer(CoAP):
    def __init__(self, host, port , multicast=False):
        CoAP.__init__(self, (host, port) , multicast)
        self.add_resource('log/', AdvancedResource())

def main():
    multicast = False
    server = CoAPServer("0.0.0.0", 3122 , multicast)
    try:
        server.listen(10)
    except KeyboardInterrupt:
        print ("Server Shutdown")
        server.close()
        print ("Exiting...")


if __name__ == '__main__':
    main()
