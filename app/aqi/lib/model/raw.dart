class Raw {
  double co;
  String uid;
  int pm10;
  double so2;
  int pm25;
  double o3;
  double no2;

  Raw(
      {this.co, this.uid, this.pm10, this.so2, this.pm25, this.o3, this.no2});

  Raw.fromJson(Map<String, dynamic> json) {
    co = json['co'];
    uid = json['uid'];
    pm10 = json['pm10'];
    so2 = json['so2'];
    pm25 = json['pm25'];
    o3 = json['o3'];
    no2 = json['no2'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['co'] = this.co;
    data['uid'] = this.uid;
    data['pm10'] = this.pm10;
    data['so2'] = this.so2;
    data['pm25'] = this.pm25;
    data['o3'] = this.o3;
    data['no2'] = this.no2;
    return data;
  }
}