
import 'package:aqi/GoogleMap.dart';
import 'package:aqi/Polygon.dart';
import 'package:aqi/app_properties.dart';
import 'package:aqi/theme/colors/light_colors.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);


  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  int currentIndex = 0;

  final List<Widget> _children = [
    Polygons(),
    MapSample(),
  ];


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: _children[currentIndex],
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: currentIndex,
        showElevation: true,
        itemCornerRadius: 8,
        curve: Curves.easeInBack,
        onItemSelected: (index) => setState(() {
          print(index);
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            inactiveColor: darkGrey,
            icon: Icon(Icons.panorama_horizontal),
            title: Text("AQI Map Level Color" , style: TextStyle(fontFamily: 'Prompt'),),
            activeColor: LightColors.kGreen,
            textAlign: TextAlign.center,
          ),
          BottomNavyBarItem(
            inactiveColor: darkGrey,
            icon: Icon(Icons.map),
            title: Text('Map' , style: TextStyle(fontFamily: 'Prompt')),
            activeColor: LightColors.kGreen,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}